# County Creek Roleplay - API - Changelog

**note** All major/minor changes made to the API will be listed below.

## [v0.0.1] - 2017-12-15

### Added
- Users
    - Create/Retrieve/Update/Delete Users.
- Internal
    - Internal framework completed (Controller, Model) for reusable components.

